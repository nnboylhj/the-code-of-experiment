function flag = checkLimits(q,dq,ddq)
%CHECKLIMITS
%   Check whether the joint angle, joint speed, and joint acceleration exceed the limit

flag = false;

q_max = [ 2.8973  1.7628  2.8973 -0.0698  2.8973  3.7525  2.8973]; % Unit: rad
q_min = [-2.8973 -1.7628 -2.8973 -3.0718 -2.8973 -0.0175 -2.8973]; % Unit: rad
dq_max = [2.1750	2.1750	2.1750	2.1750	2.6100	2.6100	2.6100]; % Unit: rad/s
ddq_max = [15	7.5	10	12.5	15	20	20]; % Unit: rad/s^2

for i = 1 : size(q,1)
    a = q(i,:) < q_max;
    b = q(i,:) > q_min;
    c = dq(i,:) < dq_max;
    d = ddq(i,:) < ddq_max;
    
    if sum(a & b) ~= 7
        disp("The joint angle of group " + i + " exceeds the limit!")
        return
    elseif sum(c) ~= 7
        disp("The joint speed of group " + i + " exceeds the limit!")
        return
    elseif sum(d) ~= 7
        disp("The joint acceleration of group " + i + " exceeds the limit!")
        return
    end
end

flag = true;

end

