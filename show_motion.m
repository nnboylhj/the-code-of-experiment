close all;
clear
clc
addpath(genpath("./data"));
addpath(genpath("./urdf"));
addpath(genpath("./Utilities"));
%% Initialization work: import the robot and set up the end effector
robot = importrobot('panda_without_hand.urdf');
robot.DataFormat = 'row';
axes = show(robot,'Frames','off','Visuals','off');
axes.CameraPositionMode = 'auto';
% axes.View = [130,10];

% Add end effector frame, offset from the grip link frame
eeOffset = 0.21;
eeBody = robotics.RigidBody('end_effector');
setFixedTransform(eeBody.Joint,trvec2tform([0 0 eeOffset]));
addBody(robot,eeBody,'panda_link7');

%% Joint space planning

% The robot configuration corresponding to the three vertices of the triangle
joint_pose1 = [-0.163585427461443,-0.071528953223724,-0.210578014075616,-1.964544462646357,-0.015757890378420,1.894552946968878,0.785398163397448]; % The first group of robot configurations
joint_pose2 = [ 0.189654693140936,-0.085959949627668, 0.161447953429104,-1.981655997480769, 0.014568542442335,1.896776319441913,0.785398163397448]; % The second group of robot configurations
joint_pose3 = [ 0.066773673596151,-1.389772223871217,-0.023090804017960,-2.810368917921938,-0.022971813741300,1.420682941725020,0.785398163397448]; % The third group of robot configurations

%******************************************************
% Write your own joint space planning or Cartesian space planning related code in the following position instead of load('joint_motion.mat');
%******************************************************
% Load data
load('joint_motion.mat');
%******************************************************
%******************************************************

% size of datasss
step = size(q,1);

% Check whether the joint angle, joint speed, and joint acceleration exceed the limit
if ~exist('dq','var') || ~exist('ddq','var')
    dq = diff(q)./0.001; dq = [dq(1,:);dq];
    ddq = diff(dq)./0.001; ddq = [ddq(1,:);ddq];
end

if ~checkLimits(q,dq,ddq)
    error('check your data!')
end

%******************************************************
%******************************************************
% Save data as csv, must store joint angles (q) in rows
dlmwrite('data/joints_q.csv',q,'precision',10);
%******************************************************
%******************************************************

%% Create a set of desired wayPoints
wayPoints1 = [0.5 -0.2 0.4;
              0.5  0.2 0.4];
wayPoints2 = [0.5               0.2  0.4;
              0.5-0.2 * sqrt(3) 0    0.4];
wayPoints3 = [0.5-0.2 * sqrt(3) 0    0.4;
              0.5               -0.2 0.4];
exampleHelperPlotWaypoints(wayPoints1);
exampleHelperPlotWaypoints(wayPoints2);
exampleHelperPlotWaypoints(wayPoints3);
%% Create a smooth curve from the waypoints to serve as trajectory
trajectory1 = cscvn(wayPoints1');
trajectory2 = cscvn(wayPoints2');
trajectory3 = cscvn(wayPoints3');
% Plot trajectory spline and waypoints
hold on
fnplt(trajectory1,'r',2);
fnplt(trajectory2,'r',2);
fnplt(trajectory3,'r',2);
%% Visualize robot configurations
file_name = "./pictures/joint_moiton.gif";
save_flag = false; % whether save gif

% Joint space motion
title('Joint motion')
hold on
axis([-0.4 0.6 -0.4 0.4 0 0.8]);
for i = 1:100:step
    show(robot,q(i,:),'PreservePlot', true,'Frames','on','Visuals','on'); % show the robot
    pause(0.01) % Set playback speed
    
    % Write to the GIF File
    if save_flag == true
        frame = getframe(gcf);
        im = frame2im(frame);
        [imind,cm] = rgb2ind(im,256);
        if i == 1
            imwrite(imind,cm,file_name,'gif', 'Loopcount',inf);
        else
            imwrite(imind,cm,file_name,'gif','WriteMode','append');
        end
    end
end
%% Display joint angle, joint speed, joint acceleration
figure
subplot(3,1,1)
plot(q(:,:),'LineWidth',1)
lgd = ["Joint1","Joint2","Joint3","Joint4","Joint5","Joint6","Joint7"];
grid on
legend(lgd)
xlim([0 size(q,1)])
title('$$q$$','interpreter','latex','FontSize',10)
xlabel('$$t(ms)$$','interpreter','latex','FontSize',10)
ylabel('$$rad$$','interpreter','latex','FontSize',10)

subplot(3,1,2)
plot(dq(:,:),'LineWidth',1)
grid on
legend(lgd)
xlim([0 size(q,1)])
title('$$\dot{q}$$','interpreter','latex','FontSize',10)
xlabel('$$t(ms)$$','interpreter','latex','FontSize',10)
ylabel('$$rad/s$$','interpreter','latex','FontSize',10)

subplot(3,1,3)
plot(ddq(:,:),'LineWidth',1)
grid on
legend(lgd)
xlim([0 size(q,1)])
title('$$\ddot{q}$$','interpreter','latex','FontSize',10)
xlabel('$$t(ms)$$','interpreter','latex','FontSize',10)
ylabel('$$rad/s^2$$','interpreter','latex','FontSize',10)